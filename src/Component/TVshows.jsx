import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import DisplayTvshows from "./DisplayTvshows";
import config from "../config";

class TVshows extends Component {
  state = {
    TvshowData: [],
    searchQuery: "",
    searchResult: [],
  };

  componentDidMount() {
    axios
      .get(
        `https://api.themoviedb.org/3/tv/popular?api_key=${config.api_key}&page=1`
      )
      .then((res) => {
        console.log(res);
        const data = res.data.results;
        console.log('tv showdata', data)
        this.setState({ TvshowData: data });
      });
  }

  handleSearch = (e) => {
    this.setState({ searchQuery: e.target.value });
    const { searchQuery } = this.state
    axios
      .get(`https://api.themoviedb.org/3/search/tv?api_key=${config.api_key}&page=1&query=${searchQuery}&include_adult=false`)
      .then((res) => {
        console.log('search result', res)
        this.setState({ searchResult: res.data.results });
      });

  };

  render() {
    const { TvshowData, searchResult, searchQuery } = this.state;
    const data = searchQuery !== '' ? searchResult : TvshowData
    console.log('searchResult', searchResult)
    return (
      <div className='div'>
        <h1> Popular Tv-Shows</h1>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/TVshows">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>TVshows</span>
              </Link>
            </li>
          </ol>
        </nav>
        <input type='text' className='searchbutton' placeholder='Search' onChange={this.handleSearch} value={this.state.searchQuery} />

        {searchQuery !== '' && <p>Search Result found: {searchResult.length}</p>}

        <div className='row'>
          {data.map((showData, id) => (
            <DisplayTvshows key={id} Data={showData} />
          ))}
        </div>
      </div>
    );
  }
}
export default TVshows;
