import React, { Component } from 'react'

export default class TvshowsSearch extends Component {
  state = {
    searchQuery: "",
    searchResult: [],
  }
  handleSearch = (e) => {
    this.setState({ searchQuery: e.target.value });
    axios
      .get(
        `https://api.themoviedb.org/3/search/tv?api_key=1b035df2983fdd96a5fad1b7c802f49f&language=en-US&page=1&query=Dark&include_adult=false`
      )
      .then((response) => {
        // console.log(response, "search");

        const Result = response.data.results;
        this.setState({ searchResult: Result });
        // console.log(Result, "results");
      });
  };
  render() {
    const img_baseURL = "https://image.tmdb.org/t/p/w500/";
    return (
      <div>
        <div className='col-md-12 linkbar'>
          <Link to='/'>Tvshows</Link>
        </div>
        <input
          type='text'
          class='searchbutton'
          onChange={this.handleSearch}
          placeholder='Search'
        />
        <div className='searchList'>
          {this.state.searchResult.slice(1, 4).map((search) => (
            <Link style={{ display: "flex" }} to={`MovieDetail/${search.id}`}>
              <img
                src={`${img_baseURL}${search.poster_path}`}
                class='card-img-top'
                alt='not found'
                height='50px'
                width='40px'
              />
              <h1 className='title'> {search.original_title}</h1>
            </Link>
          ))}
        </div>
      </div>
    )
  }
}
