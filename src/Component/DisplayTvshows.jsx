import React, { Component } from "react";
import { Link } from "react-router-dom";
import config from '../config'

class DisplayTvshows extends Component {
  render() {
    const { Data: { poster_path, name, vote_average, number_of_episodes, number_of_seasons, popularity, id } } = this.props
    return (
      <div className='col-md-3'>
        <div className='card'>
          <div className='card-body'>
            {poster_path ?
              <img
                src={`${config.img_baseURL}${poster_path}`}
                className='card-img-top'
                alt={`Poster-${name}`}
              />
              :
              <img
                src={require('./poster.png')}
                className='card-img-top'
                alt={`not-found-poster-${name}`}
              />
            }
          </div>
          <div className='card-body'>
            <h2 className='movietitle'>{name}</h2>
            {/* <p className='ratingsign'>
              Rating:
              {vote_average}/10
            </p>
            <p className='popularity'>
              Popularity:{popularity}
            </p> */}
            <Link
              to={`/Tvshowsdetail/${id}`}
              className='btn btn-success'
            >
              Veiw more
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
export default DisplayTvshows;
