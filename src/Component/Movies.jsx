import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Displaymovies from "./Displaymovies";
import config from '../config'
import { DisplaySearch } from "./DisplaySearch";
class Movies extends Component {
  state = {
    Data: [],
    searchQuery: "",
    searchResult: [],
  };

  componentDidMount() {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
      )
      .then((Response) => {
        // console.log(Response);

        const Data = Response.data.results;
        // console.log(Data, "good");
        this.setState({ Data: Data });
        // console.log(Data.overview, "data");
      });
  }

  handleSearch = (e) => {
    this.setState({ searchQuery: e.target.value });
    const { searchQuery } = this.state
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${config.api_key}&language=en-US&query=${searchQuery}&page=1&include_adult=false`
      )
      .then((response) => {
        // console.log(response, "search");

        const Result = response.data.results;
        this.setState({ searchResult: Result });
        // console.log(Result, "results");
      });
  };
  render() {
    const { Data, searchResult, searchQuery } = this.state;
    const data = searchQuery !== '' ? searchResult : Data
    // console.log(this.state.searchResult, "state");
    const img_baseURL = "https://image.tmdb.org/t/p/w500/";
    return (
      <div className='div'>
        <h1> Upcoming movies</h1>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/Movies">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Movies</span>
              </Link>
            </li>
          </ol>
        </nav>
        <input
          type='text'
          class='searchbutton'
          onChange={this.handleSearch}
          placeholder='Search'
        />

        <div className='row mid-div1'>
          {data.map((Data) => (
            <Displaymovies Data={Data} />
          ))}
        </div>
      </div>
    );
  }
}
export default Movies;
