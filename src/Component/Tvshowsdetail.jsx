import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios"
import config from '../config'
class Tvshowsdetail extends Component {
  state = {
    Tvshows: [],
    trailer: ""
  };
  componentDidMount() {
    const id = this.props.match.params.id
    axios
      .get(
        `https://api.themoviedb.org/3/tv/${id}?api_key=${config.api_key}`
      )
      .then((response) => {
        console.log('response', response.data.result);
        const Tvshows = response.data;
        this.setState({ Tvshows: Tvshows });
      });
    axios
      .get(
        `https://api.themoviedb.org/3/tv/${id}/videos?api_key=${config.api_key}`
      )
      .then((response) => {
        const data = response.data.results;
        console.log(data, "video");
        this.setState({ video: data });
      });
  }
  render() {
    const { poster_path, name, vote_average, popularity, number_of_episodes, number_of_seasons, overview, id } = this.state.Tvshows
    console.log('tvsho', this.state.Tvshows)
    return <div>         <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        <li key="Homelink" className="breadcrumb-item">
          <Link to="/">
            <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>Home</span>
          </Link>
        </li>
        <li>
          <Link to="/TVshows">
            <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>TVshows</span>
          </Link>
        </li>
        <li>
          <Link to="/Moviedetails">
            <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>{name}</span>
          </Link>
        </li>
      </ol>
    </nav>
      <div className="row row1" >
        <div className="col-md-6">
          <div className='card-body'>
            {poster_path ?
              <img
                src={`${config.img_baseURL}${poster_path}`}
                className='card-img-top'
                alt={`Poster-${name}`}
                style={{ height: 450, width: 350 }}
              />
              :
              <img
                src={require('./poster.png')}
                className='card-img-top'
                alt={`not-found-poster-${name}`}
                style={{ height: 450, width: 350 }} />
            }
          </div>
        </div>
        <div className="col-md-6">
          <div className='card-body'>
            <h2 className='movietitle'>{name}</h2>
            <h4 className='ratingsign'>
              Rating:
              {vote_average}/10
            </h4>
            <h4 className='popularity'>
              Popularity:{popularity}
            </h4>
            <h4 className='number_of_episodes'>
              number_of_episodes:{number_of_episodes}
            </h4>
            <h4 className='number_of_seasons'>
              number_of_seasons:{number_of_seasons}
            </h4>
            <h5 className='overview'>
              overview:{overview}
            </h5>

          </div>
        </div>
        <div>
          <h1 style={{ color: "red", textAlign: "center" }}>Trailer</h1>
          <iframe
            height='400px'
            width='550px'
            src={`https://www.youtube.com/embed/${this.state.trailer.key}`}
          />
        </div>
      </div>
    </div>
  }
}


export default Tvshowsdetail