import React, { Component } from "react";

import { Link } from "react-router-dom";
import axios from "axios";
import config from '../config'


export class MovieDetail extends Component {
  state = {
    movie: [],
    trailer: {},
  };


  componentDidMount() {
    const id = this.props.match.params.id
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=${config.api_key}`
      )
      .then((response) => {
        const movie = response.data;
        this.setState({ movie });
      });
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${id}/videos?api_key=${config.api_key}`
      )
      .then((response) => {
        const trailer = response.data.results[0];
        this.setState({ trailer });
      });
  }
  render() {
    const { movie } = this.state
    const { poster_path, title, vote_average, popularity, release_date, overview, number_of_episodes, id } = this.state.movie
    return (
      <div>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/Movies">
                <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>Movies</span>
              </Link>
            </li>
            <li>
              <Link to="/Moviedetails">
                <span style={{ color: "black", fontWeight: "bold", padding: '3px' }}>{title}</span>
              </Link>
            </li>
          </ol>
        </nav>
        <div className="row row1">
          <div className="col-md-6">
            <div className='card-body'>
              {poster_path ?
                <img className="img"
                  src={`${config.img_baseURL}${poster_path}`}
                  className='card-img-top'
                  alt={`Poster-${title}`}
                  style={{ height: 450, width: 350 }}
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${title}`}
                  style={{ height: 450, width: 350 }}
                />
              }
            </div>
          </div>
          <div className="col-md-6">
            <div className='card-body'>
              <h2 className='movietitles'>{title}</h2>
              <h3 className='ratingsign'>
                Rating:
              {vote_average}/10
            </h3>
              <h3 className='popularity'>
                Popularity:{popularity}
              </h3>
              <h3 className='release_date'>
                release_date:{release_date}
              </h3>
              <h4 className='overview'>
                overview:{overview}
              </h4>
            </div>
          </div>
          <div className="col-md-12 trailerpart">
            <h1 style={{ color: "red", textAlign: "center" }}>Trailer</h1>
            <iframe
              height='400px'
              width='550px'
              src={`https://www.youtube.com/embed/${this.state.trailer.key}`}
            />
          </div>
        </div>
      </div >

    );
  }
}

export default MovieDetail;
