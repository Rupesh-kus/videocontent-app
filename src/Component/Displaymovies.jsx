import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

import { Moviedetail } from "./MovieDetail";

export default class Displaymovies extends Component {
  state = {
    detail: "",
  };

  render() {
    const img_baseURL = "https://image.tmdb.org/t/p/w500/";
    return (
      <>
        <div className='col-md-3'>
          <div className='card'>
            {/* <div className='card-heading'>{this.props.Data.title}</div> */}
            <div className='card-body'>
              {this.props.Data.poster_path ?
                <img
                  className='img'
                  src={`${img_baseURL}${this.props.Data.poster_path}`}
                  class='card-img-top'
                  alt='not found'
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${this.props.Data.name}`}
                />
              }

              <div class='card-body'>
                <p className='movietitle'>{this.props.Data.title}</p>
                {/* <p className='ratingsign'>
                  Rating:
                  {this.props.Data.vote_average}/10
                </p>
                <p className='popularity'>
                  Popularity:{this.props.Data.popularity}
                </p>
                <p className='release_date'>
                  release_date:{this.props.Data.release_date}
                </p> */}
                <Link
                  to={`/MovieDetail/${this.props.Data.id}`}
                  class='btn btn-success'
                >
                  Veiw more
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
