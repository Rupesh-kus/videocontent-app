import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import config from '../config'
export default class Home extends Component {
  state = {
    Data: [],
    TvshowData: [],


  };
  componentDidMount() {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${config.api_key}&language=en-US&page=1`
      )
      .then((Response) => {
        // console.log(Response);

        const Data = Response.data.results;
        // console.log(Data, "good");
        this.setState({ Data: Data });
        // console.log(Data.overview, "data");
      });

    axios
      .get(
        `https://api.themoviedb.org/3/tv/popular?api_key=${config.api_key}&page=1`
      )
      .then((res) => {
        console.log(res);
        const data = res.data.results;
        console.log('tv showdata', data)
        this.setState({ TvshowData: data });
      });
  }
  render() {
    const { Data, TvshowData } = this.state;
    console.log(this.state.Data, "Data");
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1
    };
    return (
      <div className="home">
        <nav aria-label="breadcrumb">
          <h4 style={{ textAlign: "center" }}> Video-Content-App</h4>
          <ol className="breadcrumb">
            <li key="Homelink" className="breadcrumb-item">
              <Link to="/">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Home</span>
              </Link>
            </li>
            <li>
              <Link to="/Movies">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>Movies</span>
              </Link>
            </li>
            <li>
              <Link to="/TVshows">
                <span style={{ color: "black", fontWeight: "bold", padding: '6px' }}>TVshows</span>
              </Link>
            </li>
          </ol>
        </nav>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            {Data.slice(1, 5).map((movieData, index) => (
              <div class={`carousel-item ${index === 0 ? "active" : ""}`}>
                <img className="imgStyle" src={`${config.img_baseURL}${movieData.poster_path}`} alt="..."
                />
              </div>
            ))}

          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <h2 className="heading1"> Up-comming Movies</h2>

        <div className='row'>
          {Data.slice(1, 13).map((movieData) => (
            <div className='col-md-3'>
              <div> {movieData.poster_path ?
                <img
                  className='img'
                  src={`${config.img_baseURL}${movieData.poster_path}`}
                  class='card-img-top'
                  alt='not found'
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${movieData.name}`}
                />
              }

                <p className='movietitle'>{movieData.title}</p>

                <Link
                  to={`/MovieDetail/${movieData.id}`}
                  class='btn btn-success'
                >
                  Veiw more
                </Link>

              </div>
            </div>
          ))}


        </div>
        <h2 className="heading1"> Popular Tv-shows</h2>
        <div className="row">
          {TvshowData.slice(1, 13).map((TvshowData) => (
            <div className='col-md-3'>
              {TvshowData.poster_path ?
                <img
                  src={`${config.img_baseURL}${TvshowData.poster_path}`}
                  className='card-img-top'
                  alt={`Poster-${TvshowData.name}`}
                />
                :
                <img
                  src={require('./poster.png')}
                  className='card-img-top'
                  alt={`not-found-poster-${TvshowData.name}`}
                />
              }
              <h2 className='movietitle'>{TvshowData.name}</h2>
              <Link
                to={`/Tvshowsdetail/${TvshowData.id}`}
                className='btn btn-success'
              >
                Veiw more
            </Link>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
