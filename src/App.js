import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./Component/Home";
import Movies from "./Component/Movies";
import TVshows from "./Component/TVshows";
import "./App.css";
import { MovieDetail } from "./Component/MovieDetail";
import Tvshowsdetail from "./Component/Tvshowsdetail";
// import Tvshowsdetail from "./Component/Tvshowsdetail";

class App extends Component {
  render() {
    return (
      <BrowserRouter className='main_div'>
        <Route path='/' exact component={Home} />
        <Route path='/Movies' component={Movies} />
        <Route path='/TVshows' component={TVshows} />
        <Route path='/MovieDetail/:id' component={MovieDetail} />
        <Route path='/Tvshowsdetail/:id' component={Tvshowsdetail} />

      </BrowserRouter>
    );
  }
}
export default App;
